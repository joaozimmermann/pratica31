
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
public class Pratica31 {
    private static String meuNome = "João Marcelo Zimmermann";
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1987, Calendar.DECEMBER, 15);
    private static GregorianCalendar dataAtual = new GregorianCalendar();
    private static Date data1 = new Date();
    private static long tempototal=0;
    
    public static void main(String[] args) {
        Date inicio = new Date();
        //inicio.setTime(inicio.getTime());
        System.out.println(meuNome.toUpperCase());
        System.out.println(meuNome.substring(13, 14).toUpperCase()+ meuNome.substring(14, 23).toLowerCase()
                     + ", " + meuNome.toUpperCase().charAt(0) + ". " + meuNome.toUpperCase().charAt(5) + ".");
        data1 = dataAtual.getTime();
        tempototal = data1.getTime();
        data1 = dataNascimento.getTime();
        tempototal -= data1.getTime();
        System.out.println(tempototal/86400000); 
        Date fim = new Date();
        System.out.println(fim.getTime() - inicio.getTime());
    }    
}
